import cv2
import matplotlib.pyplot as plt

def transfrome2gray(image_path: str, save_path: str, show: bool = True) -> None:

    image_gray = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2GRAY)

    try:
        cv2.imwrite(save_path, image_gray)
    except Exception as e:
        print(f"Не удалось сохранить изображение {save_path.split('/')[-1]}")

    if show:
        plt.imshow(image_gray)